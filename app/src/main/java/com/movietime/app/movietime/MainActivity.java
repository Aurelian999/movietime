package com.movietime.app.movietime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button loginButton = (Button)findViewById(R.id.loginButton);
        loginButton.setOnClickListener(
                new Button.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        Login();
                    }
                }
        );


    }

    private void  Login()
    {

        EditText usernameBox = (EditText)findViewById(R.id.userNameTextBox);
        EditText passwordBox = (EditText)findViewById(R.id.passwordTextBox);
        String username = usernameBox.getText().toString();
        String password = passwordBox.getText().toString();

        if (username.equals("admin") && password.equals("123456"))
        {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }

        //TODO do stuff here

    }
}
