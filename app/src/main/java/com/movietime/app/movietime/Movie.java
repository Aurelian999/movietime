package com.movietime.app.movietime;

import java.io.Serializable;

/**
 * Created by Aurelian on 17-Jan-17.
 */

public class Movie implements Serializable{

    private String title;
    private String description;
    private String imgUrl;
    private String rating;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Movie() {

    }

    public Movie(String title, String description, String imgUrl, String rating) {

        this.title = title;
        this.description = description;
        this.imgUrl = imgUrl;
        this.rating = rating;
    }
}
