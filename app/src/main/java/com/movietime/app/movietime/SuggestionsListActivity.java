package com.movietime.app.movietime;

/**
 * Created by Aurelian on 1/18/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class SuggestionsListActivity extends Activity implements SuggestedMovieAdapter.OnItemTap{

    private RecyclerView mRvMovies;
    private SuggestedMovieAdapter mAdapter;
    private ArrayList<Movie> movieList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_list);
        movieList=new ArrayList<>();
        /*
        movieList.add(new Movie("title1", "test", "http://www.planwallpaper.com/static/images/desktop-year-of-the-tiger-images-wallpaper.jpg", "test"));
        movieList.add(new Movie("title2", "test", "http://www.planwallpaper.com/static/images/desktop-year-of-the-tiger-images-wallpaper.jpg", "test"));
        movieList.add(new Movie("title3", "test", "http://www.planwallpaper.com/static/images/desktop-year-of-the-tiger-images-wallpaper.jpg", "test"));
        */
        mAdapter= new SuggestedMovieAdapter(movieList, this);

        initViews();
        setListeners();
    }

    private void initViews(){
        mRvMovies = (RecyclerView) findViewById(R.id.rv_list);
        mRvMovies.setAdapter(mAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRvMovies.setLayoutManager(linearLayoutManager);
    }

    private void setListeners(){

    }

    @Override
    public void onItemClick(Movie movie) {
        Intent intent = new Intent(this, MovieActivity.class);
        intent.putExtra("suggestedMovie", movie);
        startActivity(intent);
    }
}
