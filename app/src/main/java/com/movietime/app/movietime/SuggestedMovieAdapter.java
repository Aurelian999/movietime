package com.movietime.app.movietime;

/**
 * Created by Aurelian on 1/18/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aurelian on 17-Jan-17.
 */

public class SuggestedMovieAdapter extends RecyclerView.Adapter<SuggestedMovieAdapter.ViewHolder> {

    private OnItemTap onItemTap;
    private ArrayList<Movie> movies;

    public SuggestedMovieAdapter(ArrayList<Movie> movies, OnItemTap onItemTap){
        this.movies = movies;
        this.onItemTap=onItemTap;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // Inflate the custom layout
        View movieView = inflater.inflate(R.layout.item_movie, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(movieView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Movie movie = movies.get(position);

        holder.mTitle.setText(movie.getTitle());

        holder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTap.onItemClick(movie);
            }
        });

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTitle;
        public LinearLayout main;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            main = (LinearLayout) itemView.findViewById(R.id.item_movie_main);
            mTitle=(TextView) itemView.findViewById(R.id.movie_title);
        }
    }

    public void clear(){
        movies.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Movie> movieList){
        movies.addAll(movieList);
        notifyDataSetChanged();
    }

    public interface OnItemTap{
        public void onItemClick(Movie movie);
    }
}
