package com.movietime.app.movietime.api;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import com.google.gson.Gson;
import com.movietime.app.movietime.MainActivity;
import com.movietime.app.movietime.Movie;
import com.movietime.app.movietime.MovieActivity;
import com.movietime.app.movietime.MovieAdapter;
import com.movietime.app.movietime.R;
import com.movietime.app.movietime.SuggestedMovieAdapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aurelian on 1/16/2017.
 */

public class JSONTask extends AsyncTask<String,Void, List<Movie>> {
    private RecyclerView mRvMovies;
    private MovieAdapter mAdapter;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ArrayList<Movie> doInBackground(String... params) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            //URL url = new URL(params[0]);
            String urlString = "https://api.themoviedb.org/3/discover/movie?api_key=69f13794bf561bacf0009c32a8425217&language=en-US&with_genres=99999&sort_by=popularity.desc&include_adult=false&include_video=false&page=1";
            urlString = urlString.replace("99999", params[0]);

            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            String finalJson = buffer.toString();

            JSONObject parentObject = new JSONObject(finalJson);
            JSONArray parentArray = parentObject.getJSONArray("results");

            ArrayList<Movie> movieList = new ArrayList<>();

            Gson gson = new Gson();
            for (int i = 0; i < parentArray.length(); i++) {
                JSONObject finalObject = parentArray.getJSONObject(i);

                Movie Movie = gson.fromJson(finalObject.toString(), Movie.class);
                Movie.setTitle(finalObject.getString("original_title"));
                Movie.setDescription(finalObject.getString("overview"));
                Movie.setImgUrl("https://image.tmdb.org/t/p/w300" + finalObject.getString("poster_path"));
                Movie.setRating(finalObject.getString("vote_average"));

                movieList.add(Movie);
            }
            return movieList;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
/*
    @Override
    protected void onPostExecute(final ArrayList<Movie> result) {
        super.onPostExecute(result);

        if (result != null) {
            SuggestedMovieAdapter adapter = new SuggestedMovieAdapter(result, MovieAdapter.OnItemTap());
            //.setAdapter(adapter);

            lvMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MovieModel movieModel = result.get(position);
                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                    intent.putExtra("movieModel", new Gson().toJson(movieModel));
                    startActivity(intent);
                }
            });
        } else {
            // something went wrong
        }

*/

}




/*
    @Override
    public void onItemClick(Movie movie) {
        Intent intent = new Intent(this, MovieActivity.class);
        intent.putExtra("movie", movie);
        .startActivity(intent);
    }


}*/