package com.movietime.app.movietime;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Aurelian on 17-Jan-17.
 */

public class MovieActivity extends Activity {

    private TextView mTvTitle;
    private TextView mTvDescription;
    private ImageView mIvImg;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie);

        movie = (Movie)getIntent().getSerializableExtra("movie");

        initViews();
        setData();
    }

    private void initViews(){
        mTvTitle = (TextView) findViewById(R.id.movieTitle);
        mTvDescription = (TextView) findViewById(R.id.movieDescription);
        mIvImg = (ImageView) findViewById(R.id.moviePoster);
    }

    private void setData(){
        mTvTitle.setText(movie.getTitle());
        mTvDescription.setText(movie.getDescription());

        URL url = null;
        try {
            url = new URL(movie.getImgUrl());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        InputStream content = null;
        try {
            content = (InputStream)url.getContent();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Drawable image = Drawable.createFromStream(content , "src");
        mIvImg.setImageDrawable(image);
    }
}
