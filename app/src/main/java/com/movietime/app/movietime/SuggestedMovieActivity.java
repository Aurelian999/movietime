package com.movietime.app.movietime;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Aurelian on 1/18/2017.
 */

public class SuggestedMovieActivity extends Activity {

    private TextView mTvTitle;
    private TextView mTvDescription;
    private ImageView mIvImg;
    private RatingBar ratingBar;
    private Movie movie;
    private Button addToMyListButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.suggested_movie);

        movie = (Movie)getIntent().getSerializableExtra("movie");

        initViews();
        setData();
    }

    private void initViews(){
        mTvTitle = (TextView) findViewById(R.id.movieTitle);
        mTvDescription = (TextView) findViewById(R.id.movieDescription);
        addToMyListButton = (Button) findViewById(R.id.addToMyListButton);
        mIvImg = (ImageView) findViewById(R.id.moviePoster);
        ratingBar = (RatingBar) findViewById(R.id.movieRating);
    }

    private void setData(){
        Picasso.with(this)
                .load(movie.getImgUrl())
                .into(mIvImg);

        mTvTitle.setText(movie.getTitle());
        ratingBar.setRating(Float.parseFloat(movie.getRating())/2);
        mTvDescription.setText(movie.getDescription());
        addToMyListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddToMyList(movie);
            }
        });

    }


    public void AddToMyList(Movie movie)
    {
        //TODO save movie

        // check if movie is already in list before adding
    }
}
