package com.movietime.app.movietime;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.movietime.app.movietime.api.JSONTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Aurelian on 17-Jan-17.
 */

public class SuggestActivity extends Activity implements SuggestedMovieAdapter.OnItemTap{
    public static final String BASE_URL = "https://api.themoviedb.org/3/discover/movie?api_key=69f13794bf561bacf0009c32a8425217&language=en-US";

    private RecyclerView mRvSuggestions;
    private SuggestedMovieAdapter movieAdapter;
    private ArrayList<Movie> movieList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.suggest);


        movieList=new ArrayList<>();
        movieAdapter= new SuggestedMovieAdapter(movieList, this);

        initViews();
        setListeners();
    }

    private void initViews(){
        mRvSuggestions = (RecyclerView) findViewById(R.id.rv_suggestions);
        mRvSuggestions.setAdapter(movieAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRvSuggestions.setLayoutManager(linearLayoutManager);
    }

    private void setListeners(){

        Button suggestButton = (Button)findViewById(R.id.suggestButton);
        suggestButton.setOnClickListener(
                new Button.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        Suggest();

                    }
                }
        );
    }

    private void Suggest()
    {
        EditText searchTextBox = (EditText)findViewById(R.id.searchTextBox);
        String searchTerm = searchTextBox.getText().toString().trim();
        searchTerm = GenreToId(searchTerm);

        try {
            List<Movie> movies = new JSONTask().execute(searchTerm).get();
            movieAdapter.clear();
            movieAdapter.addAll(movies);
            movieAdapter.notifyDataSetChanged();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        //TODO make it work

    }

    private static String GenreToId(String genre)
    {
        String result = null;

        genre = genre.toLowerCase();
        switch (genre)
        {
            case "action": result = "28"; break;
            case "horror": result = "27"; break;
            case "adventure": result = "12"; break;
            case "animation": result = "16"; break;
            case "comedy": result = "35"; break;
            case "drama": result = "18"; break;
            case "thriller": result = "53"; break;
                default: result = "";
        }

        return result;

    }

    private static void FillList(ArrayList<Movie> movieList)
    {
        //TODO this
    }

    @Override
    public void onItemClick(Movie movie) {
        Intent intent = new Intent(this, SuggestedMovieActivity.class);
        intent.putExtra("movie", movie);
        startActivity(intent);
    }
}

