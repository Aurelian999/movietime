package com.movietime.app.movietime;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


/**
 * Created by Aurelian on 17-Jan-17.
 */

public class HomeActivity extends Activity{

    private Button getSuggestionsButton;
    private Button viewListButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userpanel);

        initViews();
        setListeners();
    }

    private void initViews(){

        viewListButton = (Button)findViewById(R.id.viewListButton);

        getSuggestionsButton = (Button)findViewById(R.id.getSuggestionsButton);
    }

    private void setListeners(){
        viewListButton.setOnClickListener(
                new Button.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        ViewList();
                    }
                }
        );

        getSuggestionsButton.setOnClickListener(
                new Button.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        GetSuggestions();
                    }
                }
        );
    }

    private void ViewList()
    {
        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);
    }

    private void GetSuggestions()
    {
        Intent intent = new Intent(this, SuggestActivity.class);
        startActivity(intent);
    }

}
